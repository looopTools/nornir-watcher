require "filewatcher"

class Watcher
  def initialize(paths)
    @paths = paths
  end

  def run
    Filewatcher.new(@paths).watch do |change|
      puts change
      # changes.each do |filename, event|
      #   puts "#{filename} #{event}"
      # end
    end
  end

  attr_reader :paths
end
